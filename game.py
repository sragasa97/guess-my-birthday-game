# from random import randint

# name = input("Hey, what's your name? \n")

# guess_year = randint(1924, 2004)
# guess_month = randint (1, 12)
# print(name, ", were you born in ", guess_month, " / ", guess_year, "? ")
# answer = input("Yes or No? ")

# if answer == "Yes":
#     print("I knew it!")
# else:
#     for loop in range(4):
#         guess_year = randint(1924, 2004)
#         guess_month = randint (1, 12)
#         print("Then were you born in ", guess_month, " / ", guess_year, "? ")
#         answer = input("Yes or No? ")
#         if answer == "Yes":
#             print("I knew it!")
#             break
#         elif loop < 3 and answer == "No":
#             print("Drat! Lemme try again!")
#         else:
#             print("Aww man, I give up!")
#             break

# Instructor Code Below
# shirts = ["red shirt", "pink shirt", "blue shirt", "pretty shirt", "ugly shirt"]

# for shirt in shirts:
#     print(shirt)

# Instructor Code Below

import time

print("Think of a number between 1 and 50.")
time.sleep(3)
allowed=["equal","higher","lower"]

guesses = []

low = 1
high = 50
computer_won=False

for guess_number in range(5):
    from random import randint
    guess = randint(low, high)

    print("I guessed ", guess)
    response = input("Is your number equal, higher, or lower? ")
    if response in allowed:
        if response == "equal":
            print("I win!")
            computer_won=True
            guesses.append([guess,"just right!"])
            break
        elif response == "higher":
            print("My guess was too low.")
            low = guess + 1
            guesses.append([guess, "too low."])
        elif response == "lower":
            print("My guess was too high.")
            high = guess - 1
            guesses.append([guess, "too high."])
    else:
        print("incorrect entry, try again later")
        break

print("I made", len(guesses), "guesses:")
for guess,result in guesses:
    print("I guessed",guess,"which was",result)

if computer_won:
    print("I WON!")
else:
    print("YOU WON!")

# import time

# # Instructions for the human
# print("Think of a number between 1 and 50")
# time.sleep(5)
# # List of allowable input
# allowed=["equal","higher","lower"]

# # Create a list to hold guesses
# guesses = []
# # Set the range of the guess
# low = 1
# high = 50
# computer_won=False

# # Guess five times
# for guess_number in range(5):
#     # Generate a random number
#     from random import randint
#     guess = randint(low, high)

#     # Ask if the number is higher/lower/equal
#     print("I guessed", guess)
#     response = input("Is your number equal, higher, or lower? ")
#     if response in allowed:
#         if response == "equal":
#             print("I win!")
#             computer_won=True
#             guesses.append([guess,"just right"])
#             break
#         elif response == "higher":
#             print("My guess was too low.")
#             low = guess + 1
#             guesses.append([guess, "too low"])
#         elif response == "lower":
#             print("My guess was too high.")
#             high = guess - 1
#             guesses.append([guess,"too high"])
#     else:
#         print('incorrect entry, try again later')
#         break

# # Print how many guesses computer made
# print("I made", len(guesses), "guesses:")
# # Each guess and the result of the guess the computer made
# for guess,result in guesses:
#     print("I guessed",guess,"which was",result)


# if computer_won: #True == True
#     print("I WON!")
# else:
#     print("YOU WON!")
